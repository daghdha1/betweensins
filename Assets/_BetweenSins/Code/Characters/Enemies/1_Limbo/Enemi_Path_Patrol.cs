﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class Enemi_Path_Patrol : MonoBehaviour
{
    public GameObject player = null;
    private NavMeshAgent navigationMeshAgent = null;
    // Puntos por los que puede patrullar este enemigo
    public Transform[] checkPoints;
    private float maxPointX; private float minPointX; private float maxPointZ; private float minPointZ;
    // Variable que actualizamos cada vez que detectemos al jugador
    public bool isDetected = false;
    // Variable que usamos para que el enemigo sea lo persistente que queramos, es decir, que te persiga más o menos
    [Range(0, 5000)]
    public int persistanceEnemy = 100;
    private int auxiliarCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        navigationMeshAgent = this.GetComponent<NavMeshAgent>();
        navigationMeshAgent.SetDestination(checkPoints[0].position);
        // Obtenemos el punto máximo y el mínimo para establecer el rango, para que si el enemigo sale de ahí al intentar perseguir al player, vuelva
        getRangeOfPoints();
    }

    // Update is called once per frame
    void Update()
    {
        if (isDetected)
        {
            navigationMeshAgent.SetDestination(player.transform.position);
            isEnemyExitOfRange();
        }
        else
        {
            if (navigationMeshAgent.remainingDistance == 0)
            {
                navigationMeshAgent.SetDestination(checkPoints[Random.Range(0, checkPoints.Length)].position);
            }
        }
    }

    private void getRangeOfPoints()
    {
        maxPointX = checkPoints[0].position.x; minPointX = checkPoints[0].position.x;
        maxPointZ = checkPoints[0].position.z; minPointZ = checkPoints[0].position.z;

        foreach (Transform point in checkPoints)
        {
            // Buscamos el punto más grande y el más pequeño en el eje de las x
            if (point.position.x > maxPointX)
            {
                maxPointX = point.position.x;
            }
            else if (minPointX > point.position.x)
            {
                minPointX = point.position.x;
            }
            // Buscamos el punto más grande y el más pequeño en el eje de las z
            if (point.position.z > maxPointZ)
            {
                maxPointZ = point.position.z;
            }
            else if (minPointZ > point.position.z)
            {
                minPointZ = point.position.z;
            }
        }
    }

    private void isEnemyExitOfRange()
    {
        if ((navigationMeshAgent.gameObject.transform.position.x > maxPointX && navigationMeshAgent.gameObject.transform.position.z > maxPointZ && auxiliarCount >= persistanceEnemy) || (navigationMeshAgent.gameObject.transform.position.x < maxPointX && navigationMeshAgent.gameObject.transform.position.z < maxPointZ && auxiliarCount >= persistanceEnemy))
        {
            auxiliarCount = 0;
            isDetected = false;
        }
        auxiliarCount = auxiliarCount + 1;
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Player")
        {
            SceneManager.LoadScene("1_Limbo_Combat");
        }
    }
}
