﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemi_Vision_Line : MonoBehaviour
{
    // Este layerMask me permite seleccionar capas. Si ponemos = 0 dice que no está nada seleccionado, si ponemos  = ~0 es lo contrario
    public LayerMask layerMask = ~0;
    // Variable de la distancia que queremos que alcancen los "rayos" de la visión de los enemigos
    [Range(0, 100)]
    public int lengthRay = 5;

    // Update is called once per frame
    void Update()
    {
        // Aquí almacenamos si hemos chocado con algo
        RaycastHit hit;

        // Miramos si chocamos contra objetos, incluso algunos que interaccionen solo en x capas
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, lengthRay, layerMask))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            this.GetComponentInParent<Enemi_Path_Patrol>().isDetected = true;
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * lengthRay, Color.white);
        }
    }
}
