﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaroMovement : MonoBehaviour
{
    public float moveSpeed = 1.0f;
    public float turnSpeed = 45.0f;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            this.transform.Translate(new Vector3(0, 0, moveSpeed * Time.deltaTime));
        }
        if (Input.GetKey(KeyCode.S))
        {
            this.transform.Translate(new Vector3(0, 0, -moveSpeed * Time.deltaTime));
        }
        if (Input.GetKey(KeyCode.A))
        {
            this.transform.Rotate(new Vector3(0, -turnSpeed * Time.deltaTime, 0));
        }
        if (Input.GetKey(KeyCode.D))
        {
            this.transform.Rotate(new Vector3(0, turnSpeed * Time.deltaTime, 0));
        }
    }
}
