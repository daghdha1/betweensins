﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Caronte_Open_Shop : MonoBehaviour
{
    private bool isDialogSistemActivated = false;
    // Variables necesarias para poder mostrar los cuadros de dialogos
    public List<string> listaTexto;
    public string nombre;
    public Sprite foto;
    public GameObject key;
    // Objetos para la tienda
    public GameObject shopPrefav = null;
    public List<GameObject> listEquipments;
    private GameObject shopObject;
    private GameObject objectSelected;
    // Variable necesaria para que no se pueda abrir el menú de pausa cuando esté este abierto
    public bool isCaronteShopOpen = false;
    // Cerrar la tienda
    private GameObject closeShop;

    private void Start()
    {
        key.GetComponent<Renderer>().enabled = false;
    }

    private void OnTriggerStay(Collider other)
    {
        // Mostramos la tecla de E
        key.GetComponent<Renderer>().enabled = true;

        if (other.gameObject.name == "Player")
        {

            // Cuando se pulse la tecla E, abrimos un dialogo
            if (Input.GetKeyDown(KeyCode.E) && !isDialogSistemActivated)
            {
                isCaronteShopOpen = true;
                GameObject.Find("Main Camera").GetComponent<DialogSistem>().generateMultipleDialogsForOneCharacter(listaTexto, nombre, foto);
                isDialogSistemActivated = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Ocultamos la tecla E
        key.GetComponent<Renderer>().enabled = false;
    }

    private void Update()
    {
        // Si el sistema de dialogos esta activado (es decir se ha llamado a la función de ejecutar dialogo) y el dialogo no está visible, significa que ya se ha acabado de ejecutar el dialogo y se ha de abrir la tienda
        if (!GameObject.Find("Main Camera").GetComponent<DialogSistem>().isDialogsVisible && isDialogSistemActivated)
        {
            isDialogSistemActivated = false;
            // Paramos el tiempo
            Time.timeScale = 0;
            // Abrimos la tienda
            shopObject = Instantiate(shopPrefav);
            showProductsInShop();
            // Ocultamos el panel lateral hasta pulsar en un objeto
            objectSelected = GameObject.Find("ObjectSelected");
            objectSelected.SetActive(false);
            // Asignamos el botón para poder cerrar el menú
            closeShop = GameObject.Find("Close");
            Button closeShopButton = closeShop.GetComponent<Button>();
            closeShopButton.onClick.AddListener(() => closeShopMenu());
        }
    }

    private void closeShopMenu()
    {
        // Paramos el tiempo
        isCaronteShopOpen = false;
        Time.timeScale = 1;
        Destroy(shopObject);
    }

    private void showProductsInShop()
    {
        // Con el bucle cogemos todos los elementos de la lista y los mostramos en la lista
        foreach(GameObject equipment in listEquipments)
        {
            // Obtenemos la lista donde mostraremos los objetos
            GameObject list = GameObject.Find("Content");
            // Instanciamos el objeto y lo ponemos como hijo de la lista
            GameObject newEquipment = Instantiate(equipment);
            newEquipment.transform.parent = list.transform;
            // Cogemos la propiedad botón del objeto para poder ponerle un listener
            Button button = newEquipment.GetComponent<Button>();
            button.onClick.AddListener(() => getTypeOfEquipment(equipment));
        }
    }

    // Hacemos esto porque el puede ser o equipamiento o accesorio
    private void getTypeOfEquipment(GameObject shopObject)
    {
        // Mostramos el panel lateral del objeto seleccionado
        objectSelected.SetActive(true);
        // Ponemos los títulos en función del objeto
        GameObject titleBenefices = GameObject.Find("EquipmentDamage");
        Text titleBeneficesText = titleBenefices.GetComponent<Text>();
        GameObject titleRequeirement = GameObject.Find("EquipmentRequirement");
        Text titleRequeirementText = titleRequeirement.GetComponent<Text>();
        if (shopObject.GetComponent<Equipment>() != null)
        {
            titleBeneficesText.text = "Item Damage: ";
            titleRequeirementText.text = "Item requirement: ";
            changeValuesOfEquipment(shopObject);
        }
        else
        {
            titleBeneficesText.text = "Accessory benefits: ";
            titleRequeirementText.text = "Accessory requirement: ";
            changeValuesOfAccessories(shopObject);
        }
    }

    private void changeValuesOfEquipment(GameObject equipment)
    {
        // Cambiamos el nombre del equipamiento
        GameObject name = GameObject.Find("EquipmentName");
        Text nameText = name.GetComponent<Text>();
        nameText.text = equipment.GetComponent<Equipment>().equipmentName;
        // Cambiamos la foto del equipamiento
        GameObject photo = GameObject.Find("EquipmentImage");
        Image photoImage = photo.GetComponent<Image>();
        photoImage.sprite = equipment.GetComponent<Equipment>().sprite;
        // Cambiamos el valor del beneficio del equipamiento
        GameObject damage = GameObject.Find("EquipmentDamageVariable");
        Text damageText = damage.GetComponent<Text>();
        damageText.text = "+ " + equipment.GetComponent<Equipment>().damage;
        // Cambiamos los requisitos del equipamiento
        GameObject requirement = GameObject.Find("EquipmentRequirementVariable");
        Text requirementText = requirement.GetComponent<Text>();
        requirementText.text = "";
        for(int i = 0; i< equipment.GetComponent<Equipment>().requirements.Count; i++)
        {
            requirementText.text = requirementText.text + " " + equipment.GetComponent<Equipment>().requirements[i];
        }
        // Cambiamos el precio del equipamiento
        GameObject buyPrice = GameObject.Find("ByButtonText");
        Button buyPriceButton = GameObject.Find("BuyButton").GetComponent<Button>();
        Text buyPriceText = buyPrice.GetComponent<Text>();
        buyPriceButton.onClick.AddListener(() => buyEquipment(equipment.GetComponent<Equipment>().price));
        buyPriceText.text = equipment.GetComponent<Equipment>().price.ToString();
    }

    private void changeValuesOfAccessories(GameObject accesories)
    {
        // Cambiamos el nombre del accesorio
        GameObject name = GameObject.Find("EquipmentName");
        Text nameText = name.GetComponent<Text>();
        nameText.text = accesories.GetComponent<Accessories>().equipmentName;
        // Cambiamos la foto del accesorio
        GameObject photo = GameObject.Find("EquipmentImage");
        Image photoImage = photo.GetComponent<Image>();
        photoImage.sprite = accesories.GetComponent<Accessories>().sprite;
        // Cambiamos la el valor del beneficio del accesorio
        GameObject damage = GameObject.Find("EquipmentDamageVariable");
        Text damageText = damage.GetComponent<Text>();
        damageText.text = "+ " + accesories.GetComponent<Accessories>().stats[0] + " " + accesories.GetComponent<Accessories>().stats[1];
        // Cambiamos los requisitos del accesorio
        GameObject requirement = GameObject.Find("EquipmentRequirementVariable");
        Text requirementText = requirement.GetComponent<Text>();
        requirementText.text = "";
        for (int i = 0; i < accesories.GetComponent<Accessories>().requirements.Count; i++)
        {
            requirementText.text = requirementText.text + " " + accesories.GetComponent<Accessories>().requirements[i];
        }
        // Cambiamos el precio del accesorio
        GameObject buyPrice = GameObject.Find("ByButtonText");
        Button buyPriceButton = GameObject.Find("BuyButton").GetComponent<Button>();
        Text buyPriceText = buyPrice.GetComponent<Text>();
        buyPriceButton.onClick.AddListener(() => buyAccesori(accesories.GetComponent<Accessories>().price));
        buyPriceText.text = accesories.GetComponent<Accessories>().price.ToString();
    }

    private void buyAccesori(int accesoriPrice)
    {
        if (accesoriPrice < GameObject.Find("Player").GetComponent<Money>().totalMoney())
        {
            GameObject.Find("Player").GetComponent<Money>().lessMoney(accesoriPrice);
        }
    }

    private void buyEquipment(int equipmentPrice)
    {
        if (equipmentPrice < GameObject.Find("Player").GetComponent<Money>().totalMoney())
        {
            GameObject.Find("Player").GetComponent<Money>().lessMoney(equipmentPrice);
        }
    }

}
