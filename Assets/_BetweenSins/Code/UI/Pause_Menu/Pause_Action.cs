﻿// I added UnityEngine.UI and .SceneManager to use the UI Buttons on the menu and to change between Scenes.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Pause_Action : MonoBehaviour
{
    //Here there are the panels that will show the respective menus when the user press one at the Pause Menu

    [SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject equipmentPanel;
    [SerializeField] private GameObject optionPanel;

    //Here I put the buttons that are going to be interactive in the Main Menu

    public Button equipmentButton;
    public Button optionsButton;
    public Button exitButton;

    //On the void Start(), I give to the buttons I have created an AddListener method, that will trigger a function when they're clicked
    void Start()
    {
        equipmentButton.onClick.AddListener(equipmentWindow);
        optionsButton.onClick.AddListener(optionsWindow);
        exitButton.onClick.AddListener(exitOption);
    }

    //EquipmentWindow() // OptionsWindow() // ExitOption() are the different functions associated with his button (equipment/options/exit) 
    private void equipmentWindow()
    {
        pausePanel.SetActive(false);
        equipmentPanel.SetActive(true);

    }

    private void optionsWindow()
    {
        pausePanel.SetActive(false);
        optionPanel.SetActive(true);

    }

    private void exitOption()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("InitMenu_Scene");
    }
}
