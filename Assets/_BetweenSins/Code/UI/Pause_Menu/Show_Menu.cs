﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Show_Menu : MonoBehaviour
{
    [SerializeField] private GameObject menuPrefav;
    private GameObject menu;

    private bool inside = false;

    // Update is called once per frame
    void Update()
    {
        if (!GameObject.Find("Caronte").GetComponent<Caronte_Open_Shop>().isCaronteShopOpen)
        {
            if (Input.GetKeyDown(KeyCode.Escape) && inside == false)
            {
                Time.timeScale = 0;
                menu = Instantiate(menuPrefav);
                inside = true;
            }
            else if (Input.GetKeyDown(KeyCode.Escape) && inside == true)
            {
                Destroy(menu);
                Time.timeScale = 1;
                inside = false;
            }
        }
    }
}
