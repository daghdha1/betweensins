﻿// I added UnityEngine.UI and .SceneManager to use the UI Buttons and to change between Scenes.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu_StartGame : MonoBehaviour
{

    //Here I put the buttons that are going to be interactive in the Main Menu

    public Button initButton;
    public Button optionsButton;
    public Button exitButton;

    //On the void Start(), I give to the buttons I have created an AddListener method, that will trigger a function when they're clicked
    void Start()
    {
        initButton.onClick.AddListener(startGame);
        optionsButton.onClick.AddListener(optionsMenu);
        exitButton.onClick.AddListener(exitGame);
    }

    //Function startGame will launch the game. In SP1, only will go to LvL1 
    private void startGame()
    {
        SceneManager.LoadScene("1_Limbo_Exploration");
    }

    //Function optionsMenu will display a menu to change the options of the game (volume, language...) and will hide the buttons defined. The menu is not defined at all, so this function is empty
    private void optionsMenu()
    {
        Debug.Log("In the future, this will show a options menu");
    }

    //Function exitGame will close the game application. In the Unity Demo, you cant close the game directly, so for checking is working, I put a debug to show is working
    private void exitGame()
    {
        Debug.Log("Closing the game");
        Application.Quit();
    }

}
