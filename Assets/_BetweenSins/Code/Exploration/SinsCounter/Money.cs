﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Money : MonoBehaviour
{
    public static int total;
    public Text text;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<SinsFragments>() != null)
        {
            Destroy(other.gameObject);
            plusMoney(other.gameObject.GetComponent<SinsFragments>().value);
        }
    }
    
    public void plusMoney(int value)
    {
        total = total + value;
        text.text = total.ToString();
    }
    
    public void lessMoney(int value)
    {
        total = total - value;
        text.text = total.ToString();
    }

    public int totalMoney()
    {
        return total;
    }
}
