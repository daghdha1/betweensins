﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogSistem : MonoBehaviour
{
    public GameObject dialogPrefav = null;
    private List<string> dialogsText = new List<string>();
    private GameObject dialogObject;
    private int numberOfDialogs = 1;
    public bool isDialogsVisible = false;
    private bool isPaused = false;

    public void generateNewDialog(string dialogText, string nameCharacter, Sprite imageCharacter)
    {
        Dialog newDialog = new Dialog(dialogText, nameCharacter, imageCharacter);
        generateDialogPrefav(newDialog);
    }

    private void generateDialogPrefav(Dialog dialogOptions)
    {
        dialogObject = Instantiate(dialogPrefav);
        // Cambiamos el nombre del personaje y el mensaje
        dialogObject.GetComponentsInChildren<Text>()[1].text = dialogOptions.DialogText;
        dialogObject.GetComponentsInChildren<Text>()[0].text = dialogOptions.NameCharacter;
        dialogObject.GetComponentInChildren<Image>().sprite = dialogOptions.ImageCharacter;
    }
    
    // Cuando un personaje tiene varias intervenciones, creamos el objeto prefav y después en un intervalo de x segundos le vamos cambiando el texto, ya que no es necesario cambiarlo todo porque el nombre y split es el mismo
    public void generateMultipleDialogsForOneCharacter(List<string> dialogs, string nameCharacter, Sprite imageCharacter)
    {
        // Guardamos las variables que vamos a necesitar
        dialogsText = dialogs;

        // Generamos el prefav en la pantalla
        generateNewDialog(dialogsText[0], nameCharacter, imageCharacter);
        isDialogsVisible = true;
    }

    void Update()
    {
        // Si el dialogo está visible y pulsamos la tecla de espacio
        if (Input.GetKeyDown(KeyCode.Space) &&  isDialogsVisible)
        {
            // Si no se han sacado todos los diálogos
            if(numberOfDialogs < dialogsText.Count)
            {
                // Si el juego no ha sido pausado, lo pausamos
                if (!isPaused)
                {
                    isPaused = true;
                    Time.timeScale = 0;
                }
                // Cambiamos el dialogo
                dialogObject.GetComponentsInChildren<Text>()[1].text = dialogsText[numberOfDialogs];
                numberOfDialogs++;
            }
            else
            {
                // Desbloqueamos el stop del juego
                isPaused = false;
                Time.timeScale = 1;
                // Declaramos la variable false, para indicar que se ha acabado de mostrar todos los dialogos
                isDialogsVisible = false;
                // Reiniciamos la variable
                numberOfDialogs = 1;
                // Destruimos el dialogo, para que no se acumulen
                Destroy(dialogObject);
            }
            
        }
    }
}
