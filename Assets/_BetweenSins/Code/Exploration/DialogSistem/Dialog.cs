﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialog : MonoBehaviour
{
    private string dialogText;
    private string nameCharacter;
    private Sprite imageCharacter;

    public string DialogText
    {
        get
        {
            return this.dialogText;
        }
        set
        {
            this.dialogText = value;
        }
    }

    public string NameCharacter
    {
        get
        {
            return this.nameCharacter;
        }
        set
        {
            this.nameCharacter = value;
        }
    }

    public Sprite ImageCharacter
    {
        get
        {
            return this.imageCharacter;
        }
        set
        {
            this.imageCharacter = value;
        }
    }

    public Dialog(string dialogText, string nameCharacter, Sprite imageCharacter)
    {
        DialogText = dialogText;
        NameCharacter = nameCharacter;
        ImageCharacter = imageCharacter;
    }
}
