﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Movement : MonoBehaviour
{
    //Properties of the camera => distanceAway is the distance on X from the camera to the player / distanceUp is the distance on Y from the camera to the player / smooth is the soft the camera goes
    private float distanceAway = 4;
    private float distanceUp = 4;
    private float smooth = 3;

    //follow is where the player will be saved
    [SerializeField] private Transform follow;

    //Properties of the smooth the camera will turn around
    private Vector3 velocityCamSmooth = Vector3.zero;
    [SerializeField] private float camSmoothDampTime = 0.1f;

    //Positions to calculate the camera movement => targetPosition is the camera Vector / idealTargetPosition is the best Vector position for the camera / positionPlayer is the player Vector / positionBeforeCollision is the position of the camera when collideswith a wall
    private Vector3 targetPosition;
    private Vector3 idealTargetPosition;
    private Vector3 positionPlayer;
    private Vector3 positionBeforeCollision;

    // collision is a bool that checks if the camera collided with a wall
    private bool collision;

    //On the Start() , I check the Player that is in the map and I assigned to the Transform Object to check after his position
    void Start()
    {
        follow = GameObject.FindWithTag("Player").transform;
    }

    //In the LateUpdate(), the function saves the player position and if there is not a collision, the camera looks to the player and follows him at the distance put at the script
    void LateUpdate()
    {
        targetPosition = follow.position + follow.up * distanceUp - follow.forward * distanceAway;

        if (collision == false)
        {
            idealTargetPosition = targetPosition;

            smoothPosition(this.transform.position, idealTargetPosition);

            transform.LookAt(follow);
        }
    }

    /*
    //For Sprint1, this is the code implemented for the not wall clipping, but is not working on the GreyBoxing. TODO : Fix it
    //In the OnTriggerEnter(), function that is active when the camera collider is triggered, checks if the collision is with a wall, and if is it, saves the actual position of the camera and the player's one
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Wall")
        {
            Debug.Log("Enter a wall");

            positionPlayer = targetPosition;
            positionBeforeCollision = idealTargetPosition;

            collision = true;

            smoothPosition(this.transform.position, positionBeforeCollision);

            transform.LookAt(follow);
        }
    }

    //In the OnTriggerStay(), the camera continues following the player attached to the wall, and if the player leaves the range of the camera, in this case 1 unit, the camera collision is put in false and the camera follows the player again
    private void OnTriggerStay(Collider other)
    {
        collision = true;

        smoothPosition(this.transform.position, positionBeforeCollision);
        transform.LookAt(follow);

        if (targetPosition.x - other.bounds.center.x >= 1 || targetPosition.z - other.bounds.center.z >= 1)
        {
            collision = false;
        }
    }
    */

    //The smoothPosition function is for making the camera movement more softer and realistic
    private void smoothPosition(Vector3 fromPos, Vector3 toPos)
    {
        this.transform.position = Vector3.SmoothDamp(fromPos, toPos, ref velocityCamSmooth, camSmoothDampTime);
    }
}
