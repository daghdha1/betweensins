using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {
    private GameObject controller;
    public int numTile;
    public List<int> adjacencyList; // Adjacency list for selectable tiles around this
    public bool current = false;
    public bool selectable = false;

    // Variables para el BFS
    public bool visited = false;
    public Tile parent = null; // Casilla a través de la que llegamos
    public int distance = 0; // Distancia del original

    /// <summary>
    /// /////////////////////////////////////////////////////// SYSTEM METHODS ///////////////////////////////////////////////////////
    /// </summary>

    /*
     * Se ejecuta una sola vez antes de que el juego empiece 
     */
    private void Awake () {
        this.adjacencyList = new List<int> ();
        this.controller = FindObjectOfType<CombatSystem> ().gameObject;
        Vector3 pos = transform.position;
        int fila = (int) (pos.z + 1.5);
        int columna = (int) (pos.x + 1.5);
        this.numTile = fila * Constants.TilesPerRow + columna;
    }

    /*
     * Se ejecuta continuamente
     */
    private void Update () {
        // Coloreamos cada casilla
        if (current) {
            GetComponent<Renderer> ().material.color = Color.magenta;
        } else if (selectable) {
            GetComponent<Renderer> ().material.color = Color.red;
        } else {
            GetComponent<Renderer> ().material.color = Color.white;
        }
    }

    /// <summary>
    /// /////////////////////////////////////////////////////// OTHER METHODS ///////////////////////////////////////////////////////
    /// </summary>

    /*
     * Hacemos clic en casilla
     */
    private void OnMouseDown () {
        controller.GetComponent<CombatSystem> ().ClickOnTile (numTile);
    }

    public Vector3 GetVector3D () {
        return this.transform.position;
    }

    /*
     * Reiniciamos la partida
     */
    public void Reset () {
        current = false;
        selectable = false;
        visited = false;
        parent = null;
        distance = 0;
    }

}