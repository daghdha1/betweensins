﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorSystem : MonoBehaviour {
    public LayerMask layerMaskMouse = ~0; // Por defecto todas las capas
    public LayerMask layerMaskCursor = ~0; // Por defecto todas las capas

    public LayerMask attackMask = 0; //Mask to detect the tiles where an attack could be performed
    public LayerMask enemyMask = 0;

    public int rayDistance = 100;
    private GameObject selectedDecal; // LastDecal of the selected tile 
    public CombatSystem combatSystem; // Combat system object
    private GameObject selectedObject;

    /// <summary>
    /// /////////////////////////////////////////////////////// SYSTEM METHODS ///////////////////////////////////////////////////////
    /// </summary>

    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        if (combatSystem._state == BattleState.PlayerTurn) {
            cursorMovement ();
            showCursorPosition ();
            objectDetection ();
            attackToEnemyDetection ();
        } else if (this.selectedDecal != null) {
            // Hides the cursor decal
            this.selectedDecal.GetComponent<MeshRenderer> ().enabled = false;
        }
    }

    /// <summary>
    /// /////////////////////////////////////////////////////// OTHER METHODS ///////////////////////////////////////////////////////
    /// </summary>

    private void characterDetection () {
        RaycastHit hit;
        if (Physics.Raycast (transform.position, transform.TransformDirection (Vector3.down), out hit, rayDistance, layerMaskCursor)) {
            GameObject go = hit.collider.gameObject;
            if ((go.CompareTag ("Ally")) && (Input.GetButton ("CombatConfirmation") || Input.GetButton ("Fire1"))) {
                if (this.selectedObject != null) {
                    GameObject.FindObjectOfType<UISystem> ().performingAttack = false; //Cancels the previous attack
                    this.selectedObject.GetComponent<CharacterSystem> ().clearLastAttack ();
                }
                this.selectedObject = go;
                FindObjectOfType<UISystem> ().DisplayAllyCommands (this.selectedObject); //Open the main comand panel for the selected character
            }
        }
    }

    /*
     * Function to let the cursor move over the scenary tiles
     * Using the Axis horizontal and vertical of the keys moves through a limit
     * or the with the mouse using a raycast to detect the position of the tile to move the cursor
     */
    private void cursorMovement () {
        // Keyboard cursor moving
        float horizontalInput = Input.GetAxis ("Horizontal");
        float verticalInput = Input.GetAxis ("Vertical");
        // Mouse cursor moving
        if (Input.GetAxis ("Mouse X") != 0 || Input.GetAxis ("Mouse Y") != 0) {
            Cursor.visible = true;
            Ray MouseRay = Camera.main.ScreenPointToRay (Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast (MouseRay, out hit, rayDistance, layerMaskMouse)) {
                GameObject tile = hit.collider.gameObject;
                transform.position = new Vector3 (tile.transform.position.x, 3, tile.transform.position.z);
            }
        }
    }

    private void showCursorPosition () {
        if (this.selectedDecal != null) {
            this.selectedDecal.GetComponent<MeshRenderer> ().enabled = false;
        }
        RaycastHit hit;
        if (Physics.Raycast (transform.position, transform.TransformDirection (Vector3.down), out hit, rayDistance, layerMaskMouse)) {
            GameObject tile = hit.collider.gameObject;
            if (this.selectedDecal != null) {
                this.selectedDecal.GetComponent<MeshRenderer> ().enabled = false;
            }
            if (tile.CompareTag ("Tile")) {
                this.selectedDecal = tile.transform.GetChild (0).gameObject;
                this.selectedDecal.GetComponent<MeshRenderer> ().enabled = true;
            }
        }
    }

    private void objectDetection () {
        RaycastHit hit;
        if (Physics.Raycast (transform.position, transform.TransformDirection (Vector3.down), out hit, rayDistance, layerMaskCursor)) {
            if (Input.GetButton ("CombatConfirmation") || Input.GetButton ("Fire1")) {
                // Object detected by the cursor raycast
                GameObject objectDetected = hit.collider.gameObject;
                UISystem UI = GameObject.Find("UI").GetComponent<UISystem>();
                switch (objectDetected.tag) {
                    case "Ally":

                        UI.DisposeAlliesList();
                        // Open the main comand panel for the selected character
                        GameObject.Find ("UI").GetComponent<UISystem> ().DisplayAllyCommands (objectDetected);
                        combatSystem.ResetTiles ();
                        closeAttackRangeDisplay ();
                        this.selectedObject = objectDetected;
                        break;
                    case "SoulsGate":
                        // Show the allie List in the UI 
                                                
                        UI.DisposeAllyCommands(); //Dispose current ally comands panel if is opend
                        UI.foccusCharacter = null;

                        UI.DisplayAlliesList ();
                        combatSystem.ResetTiles ();
                        closeAttackRangeDisplay ();
                        this.selectedObject = objectDetected;
                        break;
                }
            }
        }
    }

    private void closeAttackRangeDisplay () {
        if (this.selectedObject != null) {
            FindObjectOfType<UISystem> ().CancelCurrentAttack (); //Cancels the previous attack
            this.selectedObject.GetComponent<CharacterSystem> ().clearLastAttack ();
        }
    }

    /*
     * Function to detect when click the tiles which are "attack range" tiles
     * on click if there is an enemy character, an attack will be performed.
     */
    private void attackToEnemyDetection () {
        if (Input.GetButton ("Fire1")) {
            UISystem UI = FindObjectOfType<UISystem> ();
            if (UI.performingAttack) { //When an ally is performing an attack
                Cursor.visible = true;
                Ray MouseRay = Camera.main.ScreenPointToRay (Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast (MouseRay, out hit, rayDistance, attackMask)) {
                    // Object detected by the cursor raycast
                    GameObject tile = hit.collider.gameObject;
                    //If in the tile there is an enemy, an attack is perform
                    GameObject enemy = EnemyOnTile (tile);
                    if (enemy != null) {
                        //PERFORM AN ATTACK
                        CharacterSystem playerCharacter = this.selectedObject.GetComponent<CharacterSystem> ();
                        bool dead = enemy.GetComponent<CharacterSystem> ().Damage (playerCharacter._attack);
                        if (dead) { enemy.SetActive (false); } //If the enemy is dead, it disappears
                        playerCharacter._remActions--;
                        UI.CancelCurrentAttack (); //End Attack
                    }
                }
            }
        }
    }

    /*
     * Function to detect if on a tile, there is an Enemy
     */
    public GameObject EnemyOnTile (GameObject tile) {
        GameObject enemy = null;
        RaycastHit hit;
        Vector3 start = tile.transform.position;

        start.y = (Constants.GroundLevel - 0.1f);
        if (Physics.Raycast (start, tile.transform.TransformDirection (Vector3.up), out hit, Mathf.Infinity, enemyMask)) {
            GameObject objectDetected = hit.collider.gameObject;
            Debug.Log ("Collides with " + objectDetected.name);
            switch (objectDetected.tag) {
                case "Enemy":
                    enemy = objectDetected;
                    break;
            }
        }
        return enemy;
    }

}