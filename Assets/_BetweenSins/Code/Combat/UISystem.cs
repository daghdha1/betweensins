﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISystem : MonoBehaviour {
    public CombatSystem combatSystem;
    public GameObject AlliesList, AllyCommands, StatusPanel, ItemsPanel;
    public GameObject AllyRow; // Row to show in the list of allies
    public GameObject foccusCharacter; // Last character foccussed with the Cursor
    private int UIDepth = 0; // Depth to known in what level of UI is the user
    [HideInInspector] public bool performingAttack = false; // Shows if an ally character is performing an attack
    public int tempCharCountId = 1; // Temporal
    /// <summary>
    /// /////////////////////////////////////////////////////// SYSTEM METHODS ///////////////////////////////////////////////////////
    /// </summary>

    void Update () {
        HandleUI ();
    }

    /// <summary>
    /// /////////////////////////////////////////////////////// OTHER METHODS ///////////////////////////////////////////////////////
    /// </summary>

    public void DisplayUserUI () {
        GameObject.Find ("MainPanel").GetComponent<Canvas> ().enabled = true;
    }

    public void HideUserUI () {
        DisposeUI ();
        GameObject.Find ("MainPanel").GetComponent<Canvas> ().enabled = false;
    }

    // Shows in the UI the Status Panel
    public void DisplayStatusPanel () {
        DisposeAllyCommands ();
        this.UIDepth = 2;
        this.StatusPanel.SetActive (true);
    }
    public void DisposeStatusPanel () {
        this.StatusPanel.SetActive (false);
        DisplayAllyCommands (this.foccusCharacter);
    }

    // Shows in the UI the Items Panel
    public void DisplayItemsPanel () {
        DisposeAllyCommands ();
        this.UIDepth = 2;
        this.ItemsPanel.SetActive (true);
    }
    public void DisposeItemsPanel () {
        this.ItemsPanel.SetActive (false);
        DisplayAllyCommands (this.foccusCharacter);
    }

    // Shows in the UI the allies List 
    public void DisplayAlliesList () {
        this.UIDepth = 1;
        GenerateAllyList ();
        this.AlliesList.SetActive (true);
    }
    public void DisposeAlliesList () {
        this.UIDepth = 0;
        ClearAllyList ();
        this.AlliesList.SetActive (false);
    }

    // Shows in the UI the selected Ally commands 
    public void DisplayAllyCommands (GameObject ally) {
        
        this.foccusCharacter = ally;

        if (this.foccusCharacter != null) { 
            this.UIDepth = 1;
            this.AllyCommands.SetActive (true);
            //Configure fields
            refreshAllyCommands (this.foccusCharacter);
            //Text txt_name = GameObject.Find("lbl_name");
        }
    }

    public void DisposeAllyCommands () {
        this.UIDepth = 0;
        this.AllyCommands.SetActive (false);
    }

    // Function to refresh ally commands panel
    private void refreshAllyCommands (GameObject ally) {
        // Data Allie
        Text txt_name = GameObject.Find ("lbl_name").GetComponent<Text> ();
        txt_name.text = ally.name;


        Text txt_hp = GameObject.Find ("lbl_HP").GetComponent<Text> ();
        txt_hp.text = "HP: " + ally.GetComponent<CharacterSystem>()._currentHP.ToString();
        txt_hp.text += "/" + ally.GetComponent<CharacterSystem>()._HP.ToString();


        Text txt_sp = GameObject.Find ("lbl_SP").GetComponent<Text> ();
        txt_sp.text = "SP: " + ally.GetComponent<CharacterSystem>()._currentSP.ToString()+ "/" + ally.GetComponent<CharacterSystem>().SP.ToString();


        Text txt_rem_actions = GameObject.Find ("lbl_actions").GetComponent<Text> ();
        int remActions = ally.GetComponent<CharacterSystem> ()._remActions;
        txt_rem_actions.text = "Actions: " + remActions;

        // Button displaying
        Button cmd_move = GameObject.Find ("cmd_move").GetComponent<Button> ();
        if (remActions > 0) { cmd_move.interactable = true; } else { cmd_move.interactable = false; }

        Button cmd_attack = GameObject.Find ("cmd_attack").GetComponent<Button> ();
        if (remActions > 0) { cmd_attack.interactable = true; } else { cmd_attack.interactable = false; }
    }

    //Close every UI panel
    public void DisposeUI () {
        this.AlliesList.SetActive (false);
        this.AllyCommands.SetActive (false);
    }

    /*
     * Function to perform an ally movement
     */
    public void cmd_Move () {
        //Find selectable tiles
        this.combatSystem.FindSelectableTiles (this.foccusCharacter);
        DisposeAllyCommands ();
    }

    /*
     * Function to perform an ally attack
     */
    public void cmd_Attack () {
        //1 action less
        //this.foccusCharacter.GetComponent<CharacterSystem> ()._remActions--;

        this.foccusCharacter.GetComponent<CharacterSystem> ().calculateAttackRange ("Allies"); //For allies
        this.performingAttack = true;

        DisposeAllyCommands ();
    }

    // Generate allie list
    private void GenerateAllyList () {
        Vector3 pos = Vector3.zero;
        pos.y = -15;
        pos.x = -150;
        int margin = 0; // Default local position
        // TODO: Temp

        foreach (GameObject ally in this.combatSystem.playerCharacterList) {
            // TODO Habrá que comprobar si cada aliado esta en combate, muerto etc para mostrarse en la lista de manera diferente
            //if (!character.GetComponent<CharacterSystem>().isInBattle) {  //If character not exists
            GameObject row = Instantiate (this.AllyRow);
            row.transform.SetParent (this.AlliesList.transform);
            pos.y = pos.y + margin;
            margin -= 30;
            row.transform.localPosition = pos;
            row.tag = "UIRow";
            row.GetComponentInChildren<Text> ().text = ally.name;
            row.GetComponentInChildren<Button> ().onClick.AddListener (delegate {
                InvokeAlly (ally);
                // TODO: // De momento siempre invoca al mismo prefab Allie  (id 1-2-3)
                // Al clicar en invocar aliado no se respetan las posiciones (id) 
                // en las que aparecen los aliados en el panel debido a la asínconia
            });
        }
    }

    // Instantiates allie on the combat board
    void InvokeAlly (GameObject ally) {
        // If allies in combat don't pass the limit of allies
        if (combatSystem.alliesInCombat.Count < combatSystem.allieLimit) {
            GameObject objOfTile;
            int targetNumTile;
            do {
                // Random num Tile
                targetNumTile = Random.Range (0, (Constants.TilesPerRow * 2) - 1);
                // Get the game object of the target Tile
                objOfTile = combatSystem.boardTiles[targetNumTile];
            } while (!combatSystem.IsTileWalkable (objOfTile)); // While Tile isn't walkable (squat Tile)

            // Get the 3D position of the target Tile
            Vector3 allie3DPos = objOfTile.GetComponent<Tile> ().GetVector3D ();
            allie3DPos.y = 0;

            // Debug.Log ("Walkable --> SI");
            // Debug.Log ("Tile --> " + targetNumTile);
            // Debug.Log ("3D Position allie --> " + allie3DPos.ToString ());

            // Temporal - Se añaden atributos distintos manualmente
            ally.GetComponent<AllieMove> ().id = tempCharCountId++;
            Debug.Log ("Ally id invoked --> " + ally.GetComponent<AllieMove> ().id);

            switch (ally.GetComponent<AllieMove> ().id) {
                case 1:
                    ally.GetComponent<CharacterSystem> ().movementRangeOfAllies = ally.GetComponent<CharacterSystem> ().movementRangeOfTaro;
                    ally.GetComponent<CharacterSystem> ().attackRangeOfAllies = ally.GetComponent<CharacterSystem> ().attackRangeOfTaro;
                    break;
                case 2:
                    ally.GetComponent<CharacterSystem> ().movementRangeOfAllies = ally.GetComponent<CharacterSystem> ().movementRangeOfSkadi;
                    ally.GetComponent<CharacterSystem> ().attackRangeOfAllies = ally.GetComponent<CharacterSystem> ().attackRangeOfSkadi;
                    break;
                case 3:
                    ally.GetComponent<CharacterSystem> ().movementRangeOfAllies = ally.GetComponent<CharacterSystem> ().movementRangeOfUnknow;
                    ally.GetComponent<CharacterSystem> ().attackRangeOfAllies = ally.GetComponent<CharacterSystem> ().attackRangeOfUnknow;
                    break;
            }

            // Save current Tile of this Allie
            ally.GetComponent<AllieMove> ().currentTile = targetNumTile;

            // Add allie to the alliesInCombat List
            this.combatSystem.alliesInCombat.Add (Instantiate (
                ally,
                allie3DPos,
                Quaternion.identity));

            // Delete ally in playerCharacterList
            RemoveAllyInvoked (ally);
            // Dispose Ally list
            DisposeAlliesList ();
        } else {
            Debug.Log ("Max allies are 3");
        }
    }

    // Clear Allies list rows
    private void ClearAllyList () {
        foreach (GameObject row in GameObject.FindGameObjectsWithTag ("UIRow")) {
            Destroy (row);
        }
    }

    private void RemoveAllyInvoked (GameObject ally) {
        for (int i = 0; i < this.combatSystem.playerCharacterList.Count; i++) {
            if (this.combatSystem.playerCharacterList[i].GetComponent<AllieMove> ().id == ally.GetComponent<AllieMove> ().id) {
                this.combatSystem.playerCharacterList.RemoveAt (i);
                Debug.Log ("Ally id deleted --> " + ally.GetComponent<AllieMove> ().id);
                return;
            }
        }
    }

    // Handles de UI disposition when nested UI panels by depth
    private void HandleUI () {
        if (Input.GetButtonDown ("Cancel") || Input.GetButtonDown ("Fire2")) {
            switch (UIDepth) {
                // Nothing or turn UI
                case 0:
                    CancelCurrentAttack ();
                    combatSystem.ResetTiles ();
                    break;
                case 1: // Commands and invoke list
                    DisposeAlliesList ();
                    DisposeAllyCommands ();
                    break;
                case 2: // Items and status
                    DisposeStatusPanel ();
                    DisposeItemsPanel ();
                    break;
            }
            //Always cancel the decals
        }
    }

    public void CancelCurrentAttack () {
        if (this.foccusCharacter != null) { 
            this.foccusCharacter.GetComponent<CharacterSystem> ().clearLastAttack ();
            this.performingAttack = false;
            DisplayAllyCommands (this.foccusCharacter);
        }
    }

}