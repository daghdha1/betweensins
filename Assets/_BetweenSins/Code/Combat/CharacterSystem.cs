﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CharacterSystem : MonoBehaviour {

    // Basic common parameters
    public string _name;
    public int _level;
    public int _remActions = 1;

    //TODO: inherit and add level scaler factos on the character class  ****TEMPORAL****
    private int characterClass;
    public int _HP, SP;
    public int _currentHP, _currentSP;
    public int _attack;
    public int movementRangeOfEnemies = 4, attackRangeOfEnemies = 1;
    public int movementRangeOfAllies = -1, attackRangeOfAllies = -1;
    public int movementRangeOfTaro = 4, attackRangeOfTaro = 1;
    public int movementRangeOfSkadi = 3, attackRangeOfSkadi = 3;
    public int movementRangeOfUnknow = 3, attackRangeOfUnknow = 1;

    //References
    private CombatSystem combatSystem;

    public bool isEnemy;
    private string targetTag;
    public GameObject attackRangeDecal;

    //Attack parameters
    public List<GameObject> attackInRangeTiles;
    [HideInInspector] public List<GameObject> validTargetTiles;
    public LayerMask characterDetection;

    //Attack for an ally
    private List<GameObject> attackRangeDecals = new List<GameObject> (); // stores the decals for the range of attack displayed on the board

    //Info text 
    public GameObject textDecal;

    void Start () {
        this.combatSystem = FindObjectOfType<CombatSystem> ();

        this.attackInRangeTiles = new List<GameObject> ();
        this.validTargetTiles = new List<GameObject> ();
        //Specifies the targets depending on the band of the character
        if (isEnemy) { targetTag = "Ally"; } else { targetTag = "Enemy"; }
    }

    /* Function to get damaged, damage indicades the qty of damage to receive
     * return true when it's dead
     */
    public bool Damage (int damage) {
        _currentHP = Mathf.Max (0, _currentHP - damage);
        showDamage (damage);
        //Debug.Log(this.name+" damaged! "+_currentHP+" remaining.");
        return _currentHP == 0;
    }

    /*
     * Function to get healed a recover HP points
     */
    public void Heal (int HP) {
        _currentHP += HP;
    }

    // Function to get the tiles in which the character will be able to perform an attack
    public bool calculateAttackRange (string type) {
        clearLastAttack ();

        if (type.Equals ("Enemies")) {
            this.attackInRangeTiles = surroindingTiles (this.gameObject, this.attackRangeOfEnemies);
        } else if (type.Equals ("Allies")) {
            this.attackInRangeTiles = surroindingTiles (this.gameObject, this.attackRangeOfAllies);
        }

        if (this.attackInRangeTiles.Count > 0) {
            foreach (GameObject tile in this.attackInRangeTiles) {

                bool valid = TileAttackable (tile); //Search if there are a rival on any tile

                if (!isEnemy) { //IF is an ally, shows the range on the screen
                    Vector3 offset = new Vector3 (0, 0.1f, 0);
                    this.attackRangeDecals.Add (Instantiate (this.attackRangeDecal, tile.transform.position + offset, Quaternion.identity));
                    if (valid) {
                        //Special color
                        MeshRenderer renderer = this.attackRangeDecals[this.attackRangeDecals.Count - 1].GetComponentInChildren<MeshRenderer> ();
                        renderer.material.SetColor ("_Color", Color.red);
                    }
                }
            }
        }
        return (this.validTargetTiles.Count > 0);
    }

    // Function to attack a Character and do it damage
    public bool attackTarget (CharacterSystem target) {
        bool dead = target.Damage (this._attack);
        Debug.Log (this.name + " attacks to " + target.gameObject.name + "!!");
        this._remActions--; //for each attack, one action less;

        return dead;
    }

    // Function to clear and reset parametes of attack 
    public void clearLastAttack () {
        this.attackInRangeTiles.Clear ();
        this.validTargetTiles.Clear ();

        foreach (GameObject tile in this.attackRangeDecals) {
            Destroy (tile);
        }
        this.attackRangeDecals.Clear ();
    }

    /*
     * Function to retrieve a cGameObject surroinding tiles
     */
    private List<GameObject> surroindingTiles (GameObject character, int range) {
        List<GameObject> tiles = new List<GameObject> ();

        Vector3 pos = character.transform.position; //Reference Vector3 to search 
        pos.y = Constants.GroundLevel; //Ground level as reference to search for tiles

        Vector3 tmp; // temporal Vector3 used for displace reference vector
        GameObject c_tile; //GameObject to store tiles in succesful cases

        for (int i = 1; i <= range; i++) {

            //Exist tile to the right
            tmp = pos;
            tmp.x = tmp.x + (Constants.TileSize) * i;
            c_tile = combatSystem.searchTileFromVector3 (tmp);
            if (c_tile != null) { tiles.Add (c_tile); }

            //Exist tile to the left
            tmp = pos;
            tmp.x = tmp.x - (Constants.TileSize) * i;
            c_tile = combatSystem.searchTileFromVector3 (tmp);
            if (c_tile != null) { tiles.Add (c_tile); }

            //Exist tile to forward
            tmp = pos;
            tmp.z = tmp.z + (Constants.TileSize) * i;
            c_tile = combatSystem.searchTileFromVector3 (tmp);
            if (c_tile != null) { tiles.Add (c_tile); }

            //Exist tile to backward
            tmp = pos;
            tmp.z = tmp.z - (Constants.TileSize) * i;
            c_tile = combatSystem.searchTileFromVector3 (tmp);
            if (c_tile != null) { tiles.Add (c_tile); }

        }

        return tiles;
    }

    /*
     *  Load the list of tiles that would be valid to be attacked by the character
     */
    private bool TileAttackable (GameObject tile) {
        bool attackable = false;

        RaycastHit hit;
        Vector3 start = tile.transform.position;
        start.y = (Constants.GroundLevel - 0.1f);
        if (Physics.Raycast (start, tile.transform.TransformDirection (Vector3.up), out hit, Mathf.Infinity, characterDetection)) {
            bool gate = false;
            GameObject objectDetected = hit.collider.gameObject;
            if (this.targetTag == "Ally") {
                if (objectDetected.tag == "SoulsGate"); { gate = true; }
            }

            //if collision detected with an enemy object, add to the List of valid targets
            if ((objectDetected.CompareTag (this.targetTag) || gate) &&
                (objectDetected.GetComponent<CharacterSystem> ().isEnemy != this.isEnemy)) {

                this.validTargetTiles.Add (hit.collider.gameObject);
                attackable = true;
            }
        }
        return attackable;
    }

    ////// AFECTIONS //////////

    private void showDamage (int damage) {
        Vector3 pos = transform.position;
        pos.y += 2;

        GameObject go = Instantiate (this.textDecal, pos, Quaternion.identity, transform);

        go.GetComponent<TextMeshPro> ().text = "-" + damage.ToString ();
    }

}