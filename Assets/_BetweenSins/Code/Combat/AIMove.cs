﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIMove : MonoBehaviour {

    private CharacterSystem characterSystem;
    private CombatSystem combatSystem;
    public GameObject currentTarget; //target to search
    public Vector3 destination; //destination to reach the target
    public List<GameObject> possibleTiles; //walkable tiles surronding the target
    public int AIPersistance = 3; //Node search depth

    //Path parametes
    public Transform[] checkpoints; //points of the path
    public Transform checkPointAvatar; //avatar of checkpoints
    private NavMeshAgent agent; //Nav mesh agent
    public float nm_threshold = 0.5f; // point arrival threshold
    private int moves = 0; //Number of movements performed (for the coroutine)
    public float moveDelay = 0.0001f; //delay between moves
    private bool lastMoveValid = true;
    private Vector3 lastPointPos; //stores the last calculated vector3 position
    private float tileSize = Constants.TileSize; //Tiles size to calculate pathfinding properly

    private void Start () {

        //Combat and character systems and Nav. mesh agent 
        this.characterSystem = GetComponent<CharacterSystem> ();
        this.combatSystem = FindObjectOfType<CombatSystem> ();
        this.agent = GetComponent<NavMeshAgent> ();

        this.AIPersistance = 3; //Search depth limit 
        this.lastPointPos = transform.position; //Last PF point created position
    }

    /*
     * Function to order the enemy to move
     * Returning if the movement is valid
     */
    public bool Move () {
        return pursingTarget ();
    }

    /*
     * Function that tries to choose a target character and create a PF route to pursue it
     * Return true or false if the PF is successful and possible
     */
    bool result = false;
    private bool pursingTarget () {
        this.AIPersistance = 3;
        bool valid = false;

        //First find the nearest target
        valid = FindTarget ();
        this.lastMoveValid = valid;

        if (valid) { //Finds a valid destination (AI persistance times)
            while (this.AIPersistance > 0) {
                this.AIPersistance--;
                result = FindValidDestination ();
                if (result) { break; }
            }

            //Finds a path to the destination
            if (result) {
                result = FindValidPath ();
                //Starts the movement
                if (result) { StartCoroutine (performMovement (this.moveDelay)); valid = this.lastMoveValid; } else { Debug.Log ("No valid path"); valid = false; } //Not find a valid path

            } else { Debug.Log ("No valid destination"); valid = false; } //Not finds a valid destination

        } else { Debug.Log ("No target found"); } //if not find a valid target near search for a random one

        return valid;
    }

    /*
     * Function to search the target character
     * the nearest to the gameobject
     */
    private bool FindTarget () {
        GameObject c_target = null;

        if (GameObject.FindGameObjectsWithTag ("Ally").Length > 0) { //If there are allies
            float distance = Mathf.Infinity;
            Vector3 position = transform.position;
            if (this.currentTarget == null) {
                foreach (GameObject target in GameObject.FindGameObjectsWithTag ("Ally")) {
                    Vector3 diff = target.transform.position - position;
                    float curDistance = diff.sqrMagnitude;
                    if (curDistance < distance) {
                        c_target = target;
                        distance = curDistance;
                    }
                }
            } else {
                int pos = Random.Range (0, GameObject.FindGameObjectsWithTag ("Ally").Length);
                // Debug.Log("Looking for random..."+this.gameObject.transform.position+" Looking for "+pos);
                c_target = GameObject.FindGameObjectsWithTag ("Ally") [pos];
            }
            this.currentTarget = c_target;

        } else if (GameObject.FindGameObjectWithTag ("SoulsGate") != null) { //If no allies, go for the souls gate
            c_target = GameObject.FindGameObjectWithTag ("SoulsGate");
            this.currentTarget = c_target;
        }
        return (c_target != null);
    }

    /*
     * Function to find if there are valid destinations for the selected target
     * Return true or false, if there are a valid destination
     */
    private bool FindValidDestination () {
        bool valid = false;
        int times = 2;
        while (times > 0) {
            times--;

            this.possibleTiles = surroindingTiles (this.currentTarget);

            if (this.possibleTiles.Count > 0) {
                foreach (GameObject tile in this.possibleTiles) {
                    if (combatSystem.IsTileWalkable (tile)) {
                        valid = true;

                        Vector3 pos = tile.transform.position;
                        pos.y -= Constants.GroundLevel;
                        this.destination = pos;
                        break;
                    }
                }
            }
            if (!valid) { FindTarget (); }
        }
        return valid;
    }

    /*
     * Function to find if there are valid path to a destination
     * Return true or false, if there are a valid PF
     */
    private bool FindValidPath () {
        bool valid = true;

        //////////////////////////////////////
        //////// Path generation /////////////
        //////////////////////////////////////

        Vector3 pos = transform.position;
        Vector3 des = this.destination;

        int limit = characterSystem.movementRangeOfEnemies;

        float v_diff = (float) (System.Math.Round (des.x, 1) - System.Math.Round (pos.x, 1)); // Vertical Distance
        float h_diff = (float) (System.Math.Round (des.z, 1) - System.Math.Round (pos.z, 1)); // Horizontal Distance

        //Amount of checkpoints, absolute values of differences / each tile size
        int steps, v_steps, h_steps;

        v_steps = (int) (Mathf.Abs (v_diff) / this.tileSize);
        h_steps = (int) (Mathf.Abs (h_diff) / this.tileSize);
        steps = (v_steps + h_steps);

        this.checkpoints = new Transform[limit];
        valid = GeneratePoints (steps, v_steps, h_steps, v_diff, h_diff, limit);
        //When no points

        if (this.checkpoints.Length < 1) { valid = false; }

        return valid;
    }

    /*
     * Function to calculate PF points from the current position to the target
     * Return true or false, if there is valid the points route of the PF
     */
    private bool GeneratePoints (int steps, int verticals, int horizontals, float v_diff, float h_diff, int limit) {
        bool valid = true;
        Vector3 point;
        GameObject p;
        lastPointPos = transform.position;

        for (int i = 0; i < limit; i++) {
            bool moved = false;
            if (!moved && ((verticals > 0) && (horizontals > 0))) {
                int rnd = Random.Range (1, 2);
                switch (rnd) {
                    case 1:
                        point = GenerateVerticalPoint (lastPointPos, v_diff);
                        p = Instantiate (this.checkPointAvatar.gameObject, point, Quaternion.identity);
                        if (combatSystem.IsTileWalkable (p)) {
                            this.checkpoints[i] = p.transform;
                            moved = true;
                            this.lastPointPos = p.transform.position; //update the last position calculated

                        } else { Destroy (p); valid = false; break; }
                        verticals--;

                        break;
                    case 2:
                        point = GenerateHorizontalPoint (lastPointPos, h_diff);
                        p = Instantiate (this.checkPointAvatar.gameObject, point, Quaternion.identity);
                        if (combatSystem.IsTileWalkable (p)) {
                            this.checkpoints[i] = p.transform;
                            moved = true;
                            this.lastPointPos = p.transform.position; //update the last position calculated
                        } else { Destroy (p); valid = false; break; }
                        horizontals--;

                        break;
                }
            }
            if (verticals > 0 && !moved) {
                point = GenerateVerticalPoint (lastPointPos, v_diff);
                p = Instantiate (this.checkPointAvatar.gameObject, point, Quaternion.identity);
                if (combatSystem.IsTileWalkable (p)) {
                    this.checkpoints[i] = p.transform;
                    moved = true;
                    this.lastPointPos = p.transform.position; //update the last position calculated

                } else { Destroy (p); valid = false; break; }
                verticals--;
            }

            if (horizontals > 0 && !moved) {
                point = GenerateHorizontalPoint (lastPointPos, h_diff);
                p = Instantiate (this.checkPointAvatar.gameObject, point, Quaternion.identity);
                if (combatSystem.IsTileWalkable (p)) {
                    this.checkpoints[i] = p.transform;
                    moved = true;
                    this.lastPointPos = p.transform.position; //update the last position calculated
                } else { Destroy (p); valid = false; break; }
                horizontals--;
            }
        }
        return valid;
    }

    /*
     * Function that returns a vector displaced on the Z axis, depending of the difference from the
     * current position. Used to calculate the next point in a PF, horizontally
     */
    private Vector3 GenerateHorizontalPoint (Vector3 m_pos, float h_diff) {
        Vector3 point;
        if (h_diff > 0) { m_pos.z = m_pos.z + this.tileSize; } else { m_pos.z = m_pos.z - this.tileSize; }
        point = m_pos;
        return point;
    }

    /*
     * Function that returns a vector displaced on the X axis, depending of the difference from the
     * current position. Used to calculate the next point in a PF, horizontally
     */
    private Vector3 GenerateVerticalPoint (Vector3 m_pos, float v_diff) {
        Vector3 point;
        if (v_diff > 0) { m_pos.x = m_pos.x + this.tileSize; } else { m_pos.x = m_pos.x - this.tileSize; }
        point = m_pos;
        return point;
    }

    /*
     * perform movement through NavMesh Agent and surface
     */
    private IEnumerator performMovement (float delay) {

        if (this.checkpoints.Length > 0) {
            int cursor = 0;
            this.moves = this.checkpoints.Length;

            //agent.SetDestination (checkpoints[cursor].position);
            //this.moves--;

            if (agent.pathStatus == NavMeshPathStatus.PathInvalid ||
                agent.pathStatus == NavMeshPathStatus.PathPartial) { lastMoveValid = false; }

            while (this.moves > 0) {
                if (agent.remainingDistance < nm_threshold) {
                    //Debug.Log("Remaining for " + name + ": " + agent.remainingDistance);
                    cursor++;
                    if ((this.checkpoints!= null) && (checkpoints[cursor - 1]!=null)) { 
                        agent.SetDestination (checkpoints[cursor - 1].position);
                    }
                    if (agent.pathStatus == NavMeshPathStatus.PathInvalid || agent.pathStatus == NavMeshPathStatus.PathPartial) { lastMoveValid = false; }
                    this.moves--;
                }
                yield return new WaitForSeconds (delay);
            }
        }
    }

    /*
     * Function to retrieve a character surroinding tiles position
     */
    private List<GameObject> surroindingTiles (GameObject character) {
        List<GameObject> tiles = new List<GameObject> ();

        Vector3 pos = character.transform.position;
        pos.y = Constants.GroundLevel;

        Vector3 tmp;
        GameObject c_tile;

        //Exist tile to the right
        tmp = pos;
        tmp.x = tmp.x + this.tileSize;
        c_tile = combatSystem.searchTileFromVector3 (tmp);
        if (c_tile != null) { tiles.Add (c_tile); }

        //Exist tile to the left
        tmp = pos;
        tmp.x = tmp.x - this.tileSize;
        c_tile = combatSystem.searchTileFromVector3 (tmp);
        if (c_tile != null) { tiles.Add (c_tile); }

        //Exist tile to forward
        tmp = pos;
        tmp.z = tmp.z + this.tileSize;
        c_tile = combatSystem.searchTileFromVector3 (tmp);
        if (c_tile != null) { tiles.Add (c_tile); }

        //Exist tile to backward
        tmp = pos;
        tmp.z = tmp.z - this.tileSize;
        c_tile = combatSystem.searchTileFromVector3 (tmp);
        if (c_tile != null) { tiles.Add (c_tile); }

        return tiles;
    }

}