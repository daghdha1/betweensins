﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllieMove : Movement {
    private GameObject combatSystem;

    /// <summary>
    /// /////////////////////////////////////////////////////// SYSTEM METHODS ///////////////////////////////////////////////////////
    /// </summary>

    void Start() {
        this.combatSystem = FindObjectOfType<CombatSystem>().gameObject;  
    }

    void Update() {
        if (moving) {
            Move();
        }
    }

    /// <summary>
    /// /////////////////////////////////////////////////////// OTHER METHODS ///////////////////////////////////////////////////////
    /// </summary>

    /*
     * Movimiento del aliado
     */
    public void Move() {
        // Si aún quedan casillas a las que desplazar
        if (path.Count > 0) {
            // Realiza el movimiento del aliado
            DoMove();
        } else { // Ya hemos llegado a la casilla destino
            // Variable de estado de movimiento desactivada
            moving = false;
            // Finaliza el turno
            // combatSystem.GetComponent<CombatSystem>().FinishTurn();
        }
    }

    /*
     * Reinicio del aliado
     * 
     * @param Tile t
     */
    public void Restart(Tile t) {
        currentTile = t.numTile;
        MoveToTile(t);
    }

}
