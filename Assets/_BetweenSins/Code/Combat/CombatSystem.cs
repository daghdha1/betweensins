﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CombatSystem : MonoBehaviour {

    // Current battle system state and UI system reference
    public BattleState _state;
    public UISystem _ui;
    public CursorSystem cursorSystem;
    private NavMeshSurface nv_mesh;
    public GameObject statusDisplay;

    // Character prefabs to be in the battle and board tiles
    public List<GameObject> playerCharacterList; //TODO: Should be an array with the allies
    public GameObject[] enemyCharacterList; //TODO: Should be an array of possible enemies
    public GameObject board; // Board
    public GameObject[] boardTiles = new GameObject[Constants.NumTiles];
    public LayerMask walkableRayCastMask;
    public GameObject soulsGate;
    List<int> allieSelectableTilesList = new List<int> (); // Lista de casillas seleccionables por el aliado actual

    // Allies and Enemies in combat 
    [HideInInspector] public List<GameObject> alliesInCombat; // Invoked by the player in the player turn
    [HideInInspector] public List<GameObject> enemiesInCombat; // Invoked by the IA in the enemy turn

    private int numTileClicked = -1;
    private int idAllieClicked = 0;

    // Combat parameters, wait time on each action, action limit and enemies limit
    public float waitTime = 5f;
    public int actionsLimit = 1;
    public int allieLimit, enemyLimit;

    /// <summary>
    /// /////////////////////////////////////////////////////// SYSTEM METHODS ///////////////////////////////////////////////////////
    /// </summary>

    private void Start () {
        _state = BattleState.Beginning;
        StartCoroutine (BeginBattle ());

        //GameObject go = Instantiate(this.statusDisplay);
        //go.GetComponent<TextMeshPro>().text = "Battle begins";
    }

    /// <summary>
    /// /////////////////////////////////////////////////////// OTHER METHODS ///////////////////////////////////////////////////////
    /// </summary>

    //////////////////////////////////////////////////////
    //////////// COMBAT STATES ROUTINES //////////////////
    /////////////////////////////////////////////////////

    private IEnumerator BeginBattle () {
        //TODO: Set UI parameters here when battle starts
        InitAndSaveTiles ();
        InitAdjacencyLists ();

        // Build Navigation mesh
        this.nv_mesh = FindObjectOfType<NavMeshSurface> ();
        nv_mesh.BuildNavMesh ();

        // Instantiations
        this.alliesInCombat = new List<GameObject> ();
        this.enemiesInCombat = new List<GameObject> ();
        generateEnemies (this.enemyLimit);

        // Wait
        yield return new WaitForSeconds (.5f);

        // By default, player plays first
        _state = BattleState.PlayerTurn;
        StartCoroutine (PlayerTurn ());
    }

    private IEnumerator PlayerTurn () {

        //showStatus ("Player turn");

        Text turnDisplay = GameObject.Find ("lbl_turn").GetComponent<Text> ();
        turnDisplay.text = "Player turn";

        _ui.DisplayUserUI ();
        yield break; //waits until user response
    }

    private IEnumerator EnemyTurn (int enemies) {
        Text turnDisplay = GameObject.Find ("lbl_turn").GetComponent<Text> ();
        turnDisplay.text = "Enemy turn";

        _ui.HideUserUI ();

        //Each enemy
        bool playerDead = false;
        while (enemies > 0) {
            bool valid = false; // Stores if last action was valid
            GameObject enemy = this.enemiesInCombat[enemies - 1];
            float delTime = 3f;

            CharacterSystem characterSystem = enemy.GetComponent<CharacterSystem> ();
            if (characterSystem._HP > 0) {
                while (characterSystem._remActions > 0) {
                    //TRY TO ATTACK
                    bool canAttack = false;
                    canAttack = characterSystem.calculateAttackRange ("Enemies");
                    if (canAttack) {
                        GameObject target = characterSystem.validTargetTiles[Random.Range (0, characterSystem.validTargetTiles.Count)];
                        bool dead = characterSystem.attackTarget (target.GetComponent<CharacterSystem> ());
                        if (dead) {
                            target.gameObject.SetActive (false);
                            playerDead = isEnemyVictory (); //Checks if enemies win
                        }
                        valid = true;
                    }
                    //TRY TO MOVE
                    if (!canAttack) {
                        valid = enemy.GetComponent<AIMove> ().Move ();
                        characterSystem._remActions--;
                        delTime = ((enemy.GetComponent<CharacterSystem> ().movementRangeOfEnemies) * 2) * enemy.GetComponent<AIMove> ().moveDelay + 1;
                    }
                }
                if (valid) {
                    yield return new WaitForSeconds (delTime);
                    //enemy.transform.LookAt(enemy.GetComponent<AIMove>().currentTarget.transform); //TODO: correct rotation to face the enemy
                }
                enemies--; //Debug.Log("Actions remaining "+enemies);
            } else { yield break; } //If enemy is dead
        }
        EndEnemyTurn (playerDead);
    }

    private IEnumerator PlayerLost () {

        Text turnDisplay = GameObject.Find ("lbl_turn").GetComponent<Text> ();
        turnDisplay.text = "DEFEAT";

        yield return new WaitForSeconds (this.waitTime); //waits until user response

        SceneManager.LoadScene ("1_Limbo_Exploration");
    }

    private IEnumerator PlayerWin () {

        Text turnDisplay = GameObject.Find ("lbl_turn").GetComponent<Text> ();
        turnDisplay.text = "VICTORY";

        yield return new WaitForSeconds (this.waitTime);

        SceneManager.LoadScene ("1_Limbo_Exploration");

    }

    //////////////////////////////////////////////////
    ///////// HANDLE TURNS AND INSTANTIATIONS ////////
    //////////////////////////////////////////////////

    //Function that generates enemies on the board
    private void generateEnemies (int enemies) {
        for (int i = 0; i < enemies; i++) {
            GameObject objOfTile;
            int targetNumTile;
            do {
                // Random num Tile
                targetNumTile = Random.Range (Constants.NumTiles - (Constants.TilesPerRow * 2), Constants.NumTiles - 1);
                // Get the game object of the target Tile
                objOfTile = boardTiles[targetNumTile];
            }
            while (!IsTileWalkable (objOfTile)); // While Tile isn't walkable (squat Tile)

            // Get the 3D position of the target Tile
            Vector3 enemy3DPos = objOfTile.GetComponent<Tile> ().GetVector3D ();
            enemy3DPos.y = 0;

            Debug.Log ("Walkable --> SI");
            Debug.Log ("Tile --> " + targetNumTile);
            Debug.Log ("3D Position enemy --> " + enemy3DPos.ToString ());

            this.enemiesInCombat.Add (Instantiate (
                this.enemyCharacterList[i],
                enemy3DPos,
                Quaternion.identity));
        }
    }

    /*
     * Function to end the player turn and reload enemy character actions
     */
    public void EndPlayerTurn () {
        //Check if everyActions Where consumed

        bool victory = isPlayerVictory ();

        if (!victory) {
            //For each enemy character that stills alive, enemies will have 1 action
            int enemies = 0;
            foreach (GameObject enemy in this.enemiesInCombat) {
                if (enemy.GetComponent<CharacterSystem> ()._currentHP > 0) {
                    enemy.GetComponent<CharacterSystem> ()._remActions = this.actionsLimit; //reload actions
                    enemies++;
                }
            }
            //Reset tiles
            ResetTiles ();

            //Rebuild Navigation mesh
            this.nv_mesh = FindObjectOfType<NavMeshSurface> ();
            nv_mesh.BuildNavMesh ();

            //Starts enemy turn
            _state = BattleState.EnemyTurn;
            StartCoroutine (EnemyTurn (enemies));
        } else {
            //Starts Victory
            _state = BattleState.Won;
            StartCoroutine (PlayerWin ());
        }
    }

    /*
     * Function to end the enemy turn and reload the player character actions
     */
    public void EndEnemyTurn (bool playerDead) {
        if (!playerDead) { //Pass turn to the player
            foreach (GameObject ally in this.alliesInCombat) {
                if (ally.GetComponent<CharacterSystem> ()._currentHP > 0) {
                    ally.GetComponent<CharacterSystem> ()._remActions = this.actionsLimit; //reload actions
                }
            }
            //Clear all used paths
            clearPaths ();

            //Rebuild Navigation mesh
            this.nv_mesh = FindObjectOfType<NavMeshSurface> ();
            nv_mesh.BuildNavMesh ();

            _state = BattleState.PlayerTurn;
            StartCoroutine (PlayerTurn ());
        } else {
            //Starts Defeat
            _state = BattleState.Lost;
            StartCoroutine (PlayerLost ());
        }
    }

    /*
     * Function to clear the Enemy paths points 
     */
    private void clearPaths () {
        foreach (GameObject p_point in GameObject.FindGameObjectsWithTag ("PathPoint")) {
            Destroy (p_point);
        }
    }

    //////////////////////////////////////////////////
    ///////// SEARCHES ///////////////////////////////
    //////////////////////////////////////////////////

    /*
     * Function to search if a board tile exists with a vector3 position
     */
    public GameObject searchTileFromVector3 (Vector3 position) {
        GameObject tile = null;
        foreach (GameObject c_tile in this.boardTiles) {
            if (c_tile.transform.position == position) {
                tile = c_tile;
                break;
            }
        }
        return tile;
    }

    /*
     * Function to check if the enemy has win (no allies and no souls gate)
     */
    private bool isEnemyVictory () {
        bool result = true;

        if (soulsGate.GetComponent<CharacterSystem> ()._currentHP < 1) {
            foreach (GameObject ally in this.alliesInCombat) {
                if (ally.GetComponent<CharacterSystem> ()._currentHP > 0) {
                    result = false;
                    break;
                }
            }
        } else { result = false; }
        return result;
    }

    /*
     * Function to check if the player has win
     */
    private bool isPlayerVictory () {
        bool result = true;

        foreach (GameObject enemy in this.enemiesInCombat) {
            if (enemy.GetComponent<CharacterSystem> ()._currentHP > 0) {
                result = false;
                break;
            }
        }
        return result;
    }

    //////////////////////////////////////////////////
    //////////// TILES ///////////////////////////////
    //////////////////////////////////////////////////

    /* 
     * Rellenamos el array de casillas
     */
    private void InitAndSaveTiles () {
        int numTile = 0;
        for (int i = 0; i < Constants.TilesPerRow; i++) {
            GameObject row = board.transform.GetChild (i).gameObject;
            for (int j = 0; j < Constants.TilesPerColumn; j++) {
                GameObject tile = row.transform.GetChild (j).gameObject;
                // Save Tile into boardTiles array
                boardTiles[i * Constants.TilesPerRow + j] = tile;
                // Save num of Tile into this Tile
                boardTiles[i * Constants.TilesPerRow + j].GetComponent<Tile> ().numTile = numTile++;
            }
        }
    }

    /*
     * Crea la matriz de adyacencia y la inicializamos con 0's
     * Rellena con 1's las casillas adyacentes de cada casilla en la matriz de adyacencia
     * Rellena la lista de casillas adyacentes (con los índices/posiciones de casillas adyacentes) que posee cada casilla
     */
    private void InitAdjacencyLists () {
        // Matriz de adyacencia
        int[, ] matrix = new int[Constants.NumTiles, Constants.NumTiles];

        // Inicializa matriz a 0's
        for (int row = 0; row < Constants.NumTiles; row++) {
            for (int col = 0; col < Constants.NumTiles; col++) {
                matrix[row, col] = 0;
            }
        }

        // Para cada posición, rellena con 1's las casillas adyacentes (arriba, abajo, izquierda y derecha)
        for (int i = 0; i < Constants.NumTiles; i++) {
            // Si estamos a la derecha, el módulo de i + 1 dará 0 (15 + 1 % 16 = 0, 31 + 1 % 16 = 0, ...)
            if ((i + 1) % Constants.TilesPerRow != 0) {
                matrix[i, i + 1] = 1;
            }
            // Si estamos a la izquierda, el módulo de i dará 0 (0 % 16 = 0, 16 % 16 = 0, ...)
            if (i % Constants.TilesPerRow != 0) {
                matrix[i, i - 1] = 1;
            }
            // Si estamos abajo (evitamos 0...15)
            if (i >= Constants.TilesPerRow) {
                matrix[i, i - Constants.TilesPerRow] = 1;
            }
            // Si estamos arriba (evitamos 239 - 255)
            if (i < Constants.NumTiles - Constants.TilesPerRow) {
                matrix[i, i + Constants.TilesPerRow] = 1;
            }
        }

        // Rellena la lista "adjacencyList" de cada casilla con los índices de sus casillas adyacentes
        for (int i = 0; i < Constants.NumTiles; i++) {
            for (int j = 0; j < Constants.NumTiles; j++) {
                if (matrix[i, j] == 1) {
                    boardTiles[i].GetComponent<Tile> ().adjacencyList.Add (j);
                }
            }
        }
    }

    /*
     * Acción de click en un aliado
     *     
     * @param int allie_id
     */
    public void FindSelectableTiles (GameObject ally) {
        //Reset tiles
        ResetTiles ();
        // Guardamos el id aliado seleccionado      
        idAllieClicked = ally.GetComponent<AllieMove> ().id;
        // Guardamos el número de la Tile seleccionada
        numTileClicked = ally.GetComponent<AllieMove> ().currentTile;
        // Marcamos la Tile seleccionada como clicada
        boardTiles[numTileClicked].GetComponent<Tile> ().current = true;
        // Buscamos las casillas que pueden ser seleccionadas (allie = true)
        // BFS. Los nodos seleccionables los ponemos como selectable=true
        Debug.Log ("Num Tile Clicked" + numTileClicked);
        BFS (numTileClicked, ally);
    }

    /*
     * Encuentra y colorea las casillas seleccionables y devuelve la lista de estas
     *     
     * @param int indexCurrentTile
     */
    private void BFS (int numTileClicked, GameObject ally) {
        // Seteamos el primer nodo
        boardTiles[numTileClicked].GetComponent<Tile> ().visited = true;
        boardTiles[numTileClicked].GetComponent<Tile> ().parent = null;
        boardTiles[numTileClicked].GetComponent<Tile> ().distance = 0;
        // Cola para el BFS
        Queue<Tile> nodes = new Queue<Tile> ();
        // Encolamos el primer nodo
        nodes.Enqueue (boardTiles[numTileClicked].GetComponent<Tile> ());
        Tile node = null;
        List<int> selectablePosList = new List<int> ();
        bool firstNode = true;
        while (nodes.Count != 0) {
            // Extraemos el primer nodo de la cola y exploramos sus nodos adyacentes
            node = nodes.Dequeue ();
            if (!firstNode) {
                node.selectable = true; // Lo hacemos seleccionable en el tablero
            } else {
                firstNode = false; // booleana de control desactivada
            }
            // Si el nodo siguiente es menor o igual que el máximo rango posible
            if ((node.distance + 1) <= ally.GetComponent<CharacterSystem> ().movementRangeOfAllies) {
                foreach (int adjacencyPos in node.adjacencyList) {
                    // Si no ha sido visitado
                    if (!boardTiles[adjacencyPos].GetComponent<Tile> ().visited) {
                        GameObject enemy = cursorSystem.EnemyOnTile (boardTiles[adjacencyPos]);
                        // Si no hay un aliado
                        if ((!IsAllieTile (boardTiles[adjacencyPos].GetComponent<Tile> ())) && (enemy == null) && (boardTiles[adjacencyPos].GetComponent<Tile> ().numTile != Constants.TilesPerRow / 2)) {
                            boardTiles[adjacencyPos].GetComponent<Tile> ().visited = true;
                            boardTiles[adjacencyPos].GetComponent<Tile> ().distance = node.distance + 1;
                            boardTiles[adjacencyPos].GetComponent<Tile> ().parent = node;
                            // Encolamos el nodo adyacente
                            nodes.Enqueue (boardTiles[adjacencyPos].GetComponent<Tile> ());
                            // Añadimos la posición del nodo seleccionable a la lista
                            selectablePosList.Add (adjacencyPos);
                        }

                    }
                }
            }
        }
        // Devuelve la lista de nodos seleccionables por el aliado para otros usos
        allieSelectableTilesList = selectablePosList;
    }

    /*
     * Acción de click en una casilla vacía
     *     
     * @param int numTile
     */
    public void ClickOnTile (int numTile) {
        // Si es una casilla roja (seleccionable), nos movemos
        if (boardTiles[numTile].GetComponent<Tile> ().selectable) {
            // Iniciamos el movimiento del aliado a la casilla seleccionada
            _ui.foccusCharacter.GetComponent<AllieMove> ().MoveToTile (boardTiles[numTile].GetComponent<Tile> ());
            // Guardamos la posición de la casilla donde se encuentra el aliado
            _ui.foccusCharacter.GetComponent<AllieMove> ().currentTile = boardTiles[numTile].GetComponent<Tile> ().numTile;
        } else {
            // Reseteamos las casillas de selección
            ResetTiles ();
        }
    }

    /*
     * Comprueba si la casilla está ocupada por un aliado
     */
    private bool IsAllieTile (Tile node) {
        foreach (GameObject obj in alliesInCombat) {
            if (obj.GetComponent<AllieMove> ().currentTile == node.numTile) {
                return true;
            }
        }
        return false;
    }

    /*
     *  Gets if a tile is walkable casting a ray upwards to the pathfinding
     */
    public bool IsTileWalkable (GameObject tile) {
        bool walkable = true;
        RaycastHit hit;
        Vector3 start = tile.transform.position;
        start.y = (Constants.GroundLevel - 0.1f);
        if (Physics.Raycast (start, tile.transform.TransformDirection (Vector3.up), out hit, Mathf.Infinity, walkableRayCastMask)) {
            walkable = false;
        }
        return walkable;
    }

    /*
     * Resetea cada casilla: color, padre, distancia y visitada
     */
    public void ResetTiles () {
        foreach (GameObject obj in boardTiles) {
            Tile tile = obj.GetComponent<Tile> ();
            tile.Reset ();
        }
    }

    /////////////////////////////////////////
    ////////// OTHERS ///////////////////////

    /* private void showStatus (string state) {

        //clear the previous
        foreach (GameObject decals in GameObject.FindGameObjectsWithTag ("UIDecals")) {
            Destroy (decals);
        }

        Vector3 pos = camera.ScreenToWorldPoint (new Vector3 (Screen.width / 4, Screen.height / 4, camera.nearClipPlane));
        pos.x = pos.x + 10;

        GameObject go = Instantiate (this.statusDisplay, pos, Quaternion.Euler (0, 45, 0));
        go.GetComponent<TextMeshPro> ().text = state;

    } */

}
///////////////////////////////////////
////////// COMBAT STATES //////////////
///////////////////////////////////////

public enum BattleState {
    Beginning,
    PlayerTurn,
    EnemyTurn,
    Won,
    Lost
}