﻿public static class Constants {

    /*
     * Combate
     */

    // Tablero
    public const int TilesPerRow = 16;
    public const int TilesPerColumn = 16;
    public const int NumTiles = TilesPerRow * TilesPerColumn;
    public const float TileSize = 1.5f;
    public const float GroundLevel = -.5f;

    // Características personajes
    public const int MoveSpeed = 5;

}