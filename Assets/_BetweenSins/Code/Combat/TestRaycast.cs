﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRaycast : MonoBehaviour
{


    public LayerMask cMask;

    private void Awake(){
        cMask = ~0;
    }

    // Update is called once per frame
    void Update()
    {

        

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        

        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        Vector3 start = transform.position; start.y = -.6f;
        if (Physics.Raycast(start, transform.TransformDirection(Vector3.up), out hit, Mathf.Infinity, cMask))
        {

            //Debug.Log(hit.collider.gameObject.name);
            Debug.DrawRay(start, transform.TransformDirection(Vector3.up) * hit.distance, Color.yellow);
        
        }
        else
        {
            Debug.DrawRay(start, transform.TransformDirection(Vector3.up) * 1000, Color.white);
    
        }

    }
}
