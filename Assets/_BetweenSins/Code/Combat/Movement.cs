using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Movement : MonoBehaviour {
    public int id;
    [HideInInspector]
    public int currentTile; // Casilla actual del aliado o del enemigo
    protected Stack<Tile> path = new Stack<Tile> (); // Pila de casillas

    // Variables para el movimiento
    protected bool moving = false;
    protected Vector3 heading = new Vector3 ();
    protected float moveSpeed = Constants.MoveSpeed;
    protected Vector3 velocity = new Vector3 ();

    /*
     * Movimiento a una casilla indicada
     * 
     * @param Tile tile
     */
    public void MoveToTile (Tile tile) {
        // Vacíamos la pila
        path.Clear ();
        // Variable de estado de movimiento activada (se llama a Move() del Update de tipo de personaje (Allie - Enemy))
        moving = true;
        // Creamos la variable next para ir guardando en la pila las casillas a recorrer
        Tile next = tile;
        while (next != null) {
            // Apilamos la casilla
            path.Push (next);
            // Seleccionamos la siguiente casilla
            next = next.parent;
        }
        // 1 action less
        GetComponent<CharacterSystem> ()._remActions--;
        // Reseteamos las casillas de selección
        GameObject.Find ("CombatSystem").GetComponent<CombatSystem> ().ResetTiles ();
    }

    /*
     * Realizar movimiento
     */
    public void DoMove () {
        Tile t = path.Peek ();
        Vector3 target = t.transform.position;

        // Mantenemos la altura
        target.y = 0;

        // Comprobamos si aún no hemos llegado al centro de la casilla
        if ((Mathf.Abs (transform.position.x - t.transform.position.x) >= 0.05f) || (Mathf.Abs (transform.position.z - t.transform.position.z) >= 0.05f)) {
            CalculateHeading (target);
            SetVelocity ();

            transform.forward = heading;
            transform.position += velocity * Time.deltaTime;
        } else { // Hemos llegado al centro de la casilla         
            transform.position = target;
            path.Pop ();
        }
    }

    /*
     * Calcula 
     */
    protected void CalculateHeading (Vector3 target) {
        heading = target - transform.position;
        heading.Normalize ();
    }

    /*
     * Selecciona la velocidad
     */
    protected void SetVelocity () {
        velocity = heading * moveSpeed;
    }

}