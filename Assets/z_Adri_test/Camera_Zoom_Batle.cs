﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Zoom_Batle : MonoBehaviour
{
    private Camera cam;
    // Movimiento cámara
    public float panSpeed = 20f;
    public float panBorderThickness = 10f;
    public Vector2 panLimitPositive;
    public Vector2 panLimitNegative;
    // Zoom cámara
    public float scrollSpeed = 20f;
    public float minZoom = 20f;
    public float maxZoom = 200f;

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;

        if (Input.mousePosition.y >= Screen.height - panBorderThickness)
        {
            pos.y += panSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.y <= panBorderThickness)
        {
            pos.y -= panSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.x >= Screen.width - panBorderThickness)
        {
            pos.x += panSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.x <= panBorderThickness)
        {
            pos.x -= panSpeed * Time.deltaTime;
        }

        if(Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if(cam.orthographicSize > minZoom)
            {
                cam.orthographicSize--;
            }
        }
        if(Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (cam.orthographicSize < maxZoom)
            {
                cam.orthographicSize++;
            }
        }

        pos.x = Mathf.Clamp(pos.x, -panLimitNegative.x, panLimitPositive.x);
        pos.y = Mathf.Clamp(pos.y, -panLimitNegative.y, panLimitPositive.y);

        transform.position = pos;
    }

    private void Start()
    {
        GameObject objectCamera = GameObject.Find("Main Camera");
        cam = objectCamera.GetComponent<Camera>();
    }
}

