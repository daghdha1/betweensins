﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment : MonoBehaviour
{
    // Start is called before the first frame update
    public string requirementsCharacter;
    public  List<string> requirements = new List<string>();
    public  int damage;
    public  int price;
    public  string equipmentName;
    public  Sprite sprite;
}
