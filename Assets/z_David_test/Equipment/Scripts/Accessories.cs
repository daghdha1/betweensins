﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accessories: MonoBehaviour
{
    public string requirementsCharacter;
    public List<string> requirements = new List<string>();
    public List<string> stats = new List<string>();
    public int price;
    public string equipmentName;
    public Sprite sprite;

}
