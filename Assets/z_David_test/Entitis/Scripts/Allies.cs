﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Allies : MonoBehaviour {

    public string pjName;
    public string pjClass;
    public int strength;
    public int dexterity;
    public int constitution;
    public int intelligencce;

    public int healt;
    public int lvl;
    public int exp;
    public int damage;
    public int crit;
    public List<string> habilitie = new List<string> ();
    public GameObject equipment;
    public GameObject accessorie;

    public void Hit (Enemies enemi) {
        enemi.healt -= this.damage;
        this.healt -= enemi.damage;
    }

    public void LvlUp () {
        int expExt = exp - (250 * lvl);
        int lvlIni = lvl;
        lvl++;

        while (expExt >= 250 * lvl) {
            expExt = expExt - (250 * lvl);
            lvl++;
        }
        exp = expExt;

        if (pjClass == "Warrior") {
            strength += 2 * (lvl - lvlIni);
            dexterity += 1 * (lvl - lvlIni);
            constitution += 2 * (lvl - lvlIni);
            intelligencce += 1 * (lvl - lvlIni);
        }
        if (pjClass == "Ranger") {
            strength += 2 * (lvl - lvlIni);
            dexterity += 2 * (lvl - lvlIni);
            constitution += 1 * (lvl - lvlIni);
            intelligencce += 1 * (lvl - lvlIni);
        }
        if (pjClass == "Mage") {
            strength += 1 * (lvl - lvlIni);
            dexterity += 1 * (lvl - lvlIni);
            constitution += 1 * (lvl - lvlIni);
            intelligencce += 3 * (lvl - lvlIni);
        }
        if (pjClass == "Healer") {
            strength += 1 * (lvl - lvlIni);
            dexterity += 1 * (lvl - lvlIni);
            constitution += 2 * (lvl - lvlIni);
            intelligencce += 2 * (lvl - lvlIni);
        }
    }
}